#!/bin/bash

ID=$1
SERVER_URL=$2
FILE_PATH="${SERVER_URL}/arm_${ID}.xml"
MODELS_URL="${SERVER_URL}/models/${ID}"

JSON_STRING=$( jq -n \
        --arg id $ID \
        --arg ModelUrl $FILE_PATH \
          '{id:$id|tonumber, ModelUrl:$ModelUrl }' )

# echo -e "PUT \n $JSON_STRING \n"

curl -X PUT  "$MODELS_URL" \
     -H 'Content-Type: application/json' \
     --data-binary @- << EOF
$JSON_STRING
EOF