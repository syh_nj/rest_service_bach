ID=1
SERVER_URL="$1/results"

JSON_STRING=$( curl -s "$SERVER_URL/$ID" )
# echo $JSON_STRING

if [ "${JSON_STRING:-0}" == {} ]
then
    bash postResult.sh $ID $1
else
    bash putResult.sh $ID $1
fi