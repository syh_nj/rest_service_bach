trim() {
    echo "$1" | tr -d '[:space:]'
}

PORT=3000
SERVER_URL="http://localhost:${PORT}"

RESPONSE=$( curl -s -o /dev/null -w "%{http_code}" "$SERVER_URL" )
# echo $SERVER_URL

if [ $RESPONSE != 200 ]
then
    json-server --watch arm_db.json --host "0.0.0.0" --port $PORT --static ./out_xml --routes routes.json &
fi

sleep 2
RESPONSE=$( curl -s -o /dev/null -w "%{http_code}" "$SERVER_URL" )
while [ $RESPONSE != 200 ]
do
    sleep 2
    RESPONSE=$( curl -s -o /dev/null -w "%{http_code}" "$SERVER_URL" )
done

INP1="./input_json/arm_1.json"
INP2="./input_json/arm_2.json"
OLD_JSON_STRING_1="[]"
OLD_JSON_STRING_2="[]"

ID_1=1
ID_2=1

while [ 1 ]
do 
    JSON_STRING_1=$( curl -s "$SERVER_URL/postInput1/${ID_1}")
    JSON_STRING_2=$( curl -s "$SERVER_URL/postInput2/${ID_2}")

    if [ "${JSON_STRING_1:-0}" == null ] || [ "${JSON_STRING_2:-0}"  == null ] || [ "${JSON_STRING_1:-0}" == {} ] || [ "${JSON_STRING_2:-0}"  == {} ]
    then
        echo "no new input"
        sleep 2
        continue
    fi

    if [ "$(trim "${JSON_STRING_1:0}")" != "[]" ] && [ "$(trim "${JSON_STRING_2:-0}")" != "[]" ]
    then
        echo $JSON_STRING_1 > $INP1
        echo $JSON_STRING_2 > $INP2

        ./JSON2XML $INP1 "./out_xml/arm_1.xml"

        if [ $? -ne 0 ]; then
            echo "JSON2XML converting file 1 failed!"
            let ID_1++
            continue
        else
            # echo "JSON2XML converting file 1 succeeded!"
            bash updateModel.sh 1 ${SERVER_URL}
        fi

        ./JSON2XML $INP2 "./out_xml/arm_2.xml"

        if [ $? -ne 0 ]; then
            echo "JSON2XML converting file 2 failed!"
            let ID_2++
            continue
        else
            # echo "JSON2XML converting file 2 succeeded!"
            bash updateModel.sh 2 ${SERVER_URL}
        fi

        echo "-1" > verificationResult.txt
        bash updateResult.sh ${SERVER_URL}
        ./arm_bach_api -wlkata $INP1 $INP2 &
		# sleep 0.5

        # bash updateResult.sh $ID ${SERVER_URL}
        wait $!
        if [ $? -ne 0 ]; then
            echo "BACH failed!"
        else
            # echo "BACH succeed!"
            # bash updateResult.sh $ID ${SERVER_URL}
            bash updateResult.sh ${SERVER_URL}
        fi
        let ID_1++
        let ID_2++
    else
        echo "empty input"
    fi
    OLD_JSON_STRING_1=${JSON_STRING_1}
    OLD_JSON_STRING_2=${JSON_STRING_2}
    sleep 2
done
