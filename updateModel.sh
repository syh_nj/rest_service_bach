ID=$1
SERVER_URL="$2/models"

JSON_STRING=$( curl -s "$SERVER_URL/$ID" )
# echo $JSON_STRING

if [ "${JSON_STRING:-0}" == {} ]
then
    bash postModel.sh $ID $2
else
    bash putModel.sh $ID $2
fi