#!/bin/bash
filename='./verificationResult.txt'
read -r firstline < $filename

ID=$1
VERIFICATION_RESULT=$((firstline + 0))
SERVER_URL="$2/results"

JSON_STRING=$( jq -n \
        --arg id $ID \
        --arg VerificationResult $VERIFICATION_RESULT \
          '{id:$id|tonumber, VerificationResult:$VerificationResult|tonumber }' )

# echo -e "POST \n $JSON_STRING \n"

curl -X POST $SERVER_URL \
     -H 'Content-Type: application/json' \
     --data-binary @- << EOF
$JSON_STRING
EOF